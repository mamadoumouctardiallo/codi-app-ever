<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211210212316 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE parrainage ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE parrainage ADD UNIQUE INDEX UNIQ_195BAFB5A76ED395 (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE parrainage DROP INDEX UNIQ_195BAFB5A76ED395 (user_id)');
        $this->addSql('ALTER TABLE parrainage DROP user_id');
    }
}
