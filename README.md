# codi-app-ever

Ceci est une application qui va permettre le CODI de bien gérer les intégrations des filleuls les prochaines années.

## Getting started

Pour faire fonctionner le projet, il faut avoir PHP de disponible sur son terminale et composer. 

Apres avoir télécharger le projet, il faut exécuter ces commandes:
```
composer install
php bin/console doctrine:database:create
php bin/console doctrine:migration:migrate
``
Apres avoir exécuter ces commandes, il faut aller dans sons serveur et voir le resultat.
